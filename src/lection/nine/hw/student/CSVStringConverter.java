package lection.nine.hw.student;

import lection.nine.hw.Student;
import lection.nine.hw.enums.Gender;

public class CSVStringConverter implements StringConverter{

    @Override
    public String toStringRepresentation(Student student) {
        return student.getName() + "," +
                student.getLastName() + "," +
                student.getGender() + "," +
                student.getId() + "," +
                student.getGroupName() + ".";
    }

    @Override
    public Student fromStringRepresentation(String str) {
        Student student = new Student();
        String[] studentAttribut = str.split(",");
        student.setName(studentAttribut[0]);
        student.setLastName(studentAttribut[1]);
        student.setGender(Gender.valueOf(studentAttribut[2]));
        student.setId(Integer.parseInt(studentAttribut[3]));
        student.setGroupName(studentAttribut[4]);

        return student;
    }
}
