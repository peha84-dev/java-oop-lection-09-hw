package lection.nine.hw.student;

import lection.nine.hw.Student;

public interface StringConverter {
    public String toStringRepresentation (Student student);
    public Student fromStringRepresentation (String str);
}
