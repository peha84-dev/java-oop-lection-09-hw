package lection.nine.hw;

import lection.nine.hw.enums.Gender;
import lection.nine.hw.exceptions.GroupOverflowException;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String groupName = "Journalism";
        Group group = new Group(groupName);

        Student student10 = new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName);

        addStudentToGroup(group, new Student("Volodymyr", "Petrenko", Gender.MALE, 1, groupName));
        addStudentToGroup(group, new Student("Vsevolod", "Andriienko", Gender.MALE, 2, groupName));
        addStudentToGroup(group, new Student("Rostyslav", "Petruk", Gender.MALE, 3, groupName));
        addStudentToGroup(group, new Student("Liudmyla ", "Andriiuk", Gender.FEMALE, 4, groupName));
        addStudentToGroup(group, new Student("Bohdan", "Petrych", Gender.MALE, 5, groupName));
        addStudentToGroup(group, new Student("Vira", "Andriievych", Gender.FEMALE, 6, groupName));
        addStudentToGroup(group, new Student("Nadiia", "Petriv", Gender.FEMALE, 7, groupName));
        addStudentToGroup(group, new Student("Liubov", "Andriiv", Gender.FEMALE, 8, groupName));

        addStudentToGroup(group, new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName));
        System.out.println(group.areThereEquivalentStudents());
        addStudentToGroup(group, new Student("Grut", "I'm", Gender.TALKING_TREE, 11, groupName));
        System.out.println(group.areThereEquivalentStudents());

        printGroupInfo(group);

        // 1) Написать метод, который создаст список, положит в него 10
        //      элементов, затем удалит первые два и последний, а затем выведет
        //      результат на экран.
        hwList();
    }

    private static void hwList() {
        List<String> list = new ArrayList<>();
        list.add("Element 01");
        list.add("Element 02");
        list.add("Element 03");
        list.add("Element 04");
        list.add("Element 05");
        list.add("Element 06");
        list.add("Element 07");
        list.add("Element 08");
        list.add("Element 09");
        list.add("Element 10");

        System.out.println(list);

        list.remove(0);
        list.remove(0);
        list.remove(list.size() - 1);

        System.out.println(list);
    }

    private static void addStudentToGroup(Group group, Student student) {
        try {
            group.addStudent(student);
        } catch (GroupOverflowException e) {
            System.out.println("\u001B[31m" + e.getMessage() + "\u001B[0m");
        }
    }

    private static void printGroupInfo(Group group) {
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
        System.out.print("\u001B[34m" + group + "\u001B[0m");
        System.out.println("\u001B[34m" + "-------------------------------------------" + "\u001B[0m");
    }
}