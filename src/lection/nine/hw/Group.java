package lection.nine.hw;

import lection.nine.hw.comparators.StudentLastNameComparator;
import lection.nine.hw.exceptions.GroupOverflowException;
import lection.nine.hw.exceptions.StudentNotFoundException;

import java.util.*;

public class Group {

    //private Student[] students = new Student[10];
    private List<Student> students = new ArrayList<>(10);
    private String groupName;

    public Group(String groupName, List<Student> students) {
        super();
        this.groupName = groupName;
        this.students = students;
    }

    public Group(String groupName) {
        super();
        this.groupName = groupName;
    }

    public Group() {
        super();
    }

    public void sortStudentsByLastName(List<Student> students) {
        students.sort(Comparator.nullsLast(new StudentLastNameComparator()));
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void addStudent(Student student) throws GroupOverflowException {
        if (student == null) {
            return;
        }

        if (students.size() < 10) {
            students.add(student);
            student.setGroupName(groupName);
        } else
            throw new GroupOverflowException(student.getName() + " " + student.getLastName() +
                    " cannot be added to the " + this.groupName + " group as it is full!");
    }

    public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
        for (Student student : this.students) {
            if (student != null && student.getLastName().equals(lastName)) {
                return student;
            }
        }
        throw new StudentNotFoundException("Student with last name " + lastName + " not found " + this.groupName + " group.");
    }

    public boolean removeStudentByID(int id) {
        for (Student student : students) {
            if (student.getId() == id) {
                students.remove(student);
                System.out.println("Student with student card id " + id + " is removed from group " + getGroupName());
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        sortStudentsByLastName(students);
        StringBuilder sb = new StringBuilder();

        sb.append("Group:\n");
        sb.append("  groupName = '").append(groupName).append("'\n");

        for (Student student : this.students) {
            sb.append("    ").append(student != null ? student.toString() : "null").append("\n");
        }

        return  sb.toString();
    }

    public boolean areThereEquivalentStudents() {
        for (int i = 0; i < students.size() - 1; i++) {
            for (int j = i + 1; j < students.size(); j++) {
                if (students.get(i) != null && students.get(i).equals(students.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getGroupName(), getStudents());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Group group = (Group) obj;
        return Objects.equals(getGroupName(), group.getGroupName()) &&
                Objects.equals(getStudents(), group.getStudents());
    }
}
